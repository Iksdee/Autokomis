package pl.autokomis.Entity;

import javax.persistence.*;

@MappedSuperclass
abstract class AbstractRefType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column
    private String name;
    @Column
    private boolean isActive;



    public Integer getId() {
        return id;
    }

    public AbstractRefType setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public AbstractRefType setName(String name) {
        this.name = name;
        return this;
    }

    public boolean isActive() {
        return isActive;
    }

    public AbstractRefType setActive(boolean active) {
        isActive = active;
        return this;
    }

    @Override
    public String toString() {
        return "AbstractRefType{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", isActive=" + isActive +
                '}';
    }
}
