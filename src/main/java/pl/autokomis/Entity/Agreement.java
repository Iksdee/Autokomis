package pl.autokomis.Entity;


import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "agreement")
public class Agreement extends AbstractEntity{

    @Column
    private String description;

    @Column
    private BigDecimal amount;


    //Relations
    @OneToOne
    @JoinColumn(name = "agreement_type_id")
    private AgreementType agreementType;


    @ManyToOne
    @JoinColumn(name = "car_id")
    private Car car;


    @ManyToOne
    @JoinColumn(name = "person_id")
    private Person person;


    public String getDescription() {
        return description;
    }

    public Agreement setDescription(String description) {
        this.description = description;
        return this;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Agreement setAmount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }

    public AgreementType getAgreementType() {
        return agreementType;
    }

    public Agreement setAgreementType(AgreementType agreementType) {
        this.agreementType = agreementType;
        return this;
    }

    public Car getCar() {
        return car;
    }

    public Agreement setCar(Car car) {
        this.car = car;
        return this;
    }

    public Person getPerson() {
        return person;
    }

    public Agreement setPerson(Person person) {
        this.person = person;
        return this;
    }


}
