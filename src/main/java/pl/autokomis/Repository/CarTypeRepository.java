package pl.autokomis.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.autokomis.Entity.CarType;

@Repository
public interface CarTypeRepository extends JpaRepository<CarType, Integer> {
}
