package pl.autokomis.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.autokomis.Entity.AgreementType;

@Repository
public interface AgreementTypeRepository extends JpaRepository<AgreementType, Integer> {
}
