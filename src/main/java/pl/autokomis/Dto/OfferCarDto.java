package pl.autokomis.Dto;
import java.math.BigDecimal;

public class OfferCarDto {

    private Integer id;
    private String mark;
    private String model;
    private String year;
    private String fuelType;
    private int distance;
    private String description;
    private BigDecimal amount;
    private String agreementType;


    private String liabilityNumber;
    private String registartionNumber;
    private String engine;
    private int horsePower;
    private String gearBox;


    public String getAgreementType() {
        return agreementType;
    }

    public OfferCarDto setAgreementType(String agreementType) {
        this.agreementType = agreementType;
        return this;
    }

    public Integer getId() {
        return id;
    }

    public OfferCarDto setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getMark() {
        return mark;
    }

    public OfferCarDto setMark(String mark) {
        this.mark = mark;
        return this;
    }

    public String getModel() {
        return model;
    }

    public OfferCarDto setModel(String model) {
        this.model = model;
        return this;
    }

    public String getYear() {
        return year;
    }

    public OfferCarDto setYear(String year) {
        this.year = year;
        return this;
    }

    public String getFuelType() {
        return fuelType;
    }

    public OfferCarDto setFuelType(String fuelType) {
        this.fuelType = fuelType;
        return this;
    }

    public int getDistance() {
        return distance;
    }

    public OfferCarDto setDistance(int distance) {
        this.distance = distance;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public OfferCarDto setDescription(String description) {
        this.description = description;
        return this;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public OfferCarDto setAmount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }

    public String getLiabilityNumber() {
        return liabilityNumber;
    }

    public void setLiabilityNumber(String liabilityNumber) {
        this.liabilityNumber = liabilityNumber;
    }

    public String getRegistartionNumber() {
        return registartionNumber;
    }

    public void setRegistartionNumber(String registartionNumber) {
        this.registartionNumber = registartionNumber;
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public int getHorsePower() {
        return horsePower;
    }

    public void setHorsePower(int horsePower) {
        this.horsePower = horsePower;
    }

    public String getGearBox() {
        return gearBox;
    }

    public void setGearBox(String gearBox) {
        this.gearBox = gearBox;
    }

    @Override
    public String toString() {
        return "OfferCarDto{" +
                "id=" + id +
                ", mark='" + mark + '\'' +
                ", model='" + model + '\'' +
                ", year='" + year + '\'' +
                ", fuelType='" + fuelType + '\'' +
                ", distance=" + distance +
                ", description='" + description + '\'' +
                ", amount=" + amount +
                ", agreementType='" + agreementType + '\'' +
                '}';
    }
}
