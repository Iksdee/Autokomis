package pl.autokomis.Dto;


public class AgreementTypeDto {

    private Integer id;
    private String name;
    private boolean isActive;

    public AgreementTypeDto() {
    }

    public Integer getId() {
        return id;
    }

    public AgreementTypeDto setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public AgreementTypeDto setName(String name) {
        this.name = name;
        return this;
    }

    public boolean isActive() {
        return isActive;
    }

    public AgreementTypeDto setActive(boolean active) {
        isActive = active;
        return this;
    }
}
