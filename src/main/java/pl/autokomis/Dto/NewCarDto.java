package pl.autokomis.Dto;


import java.math.BigDecimal;
import java.util.Date;

public class NewCarDto {

    private Date added = new Date();

    private Integer id;
    private String vin;
    private String year;
    private String mark;
    private String model;
    private String liabilityNumber;
    private String registartionNumber;
    private String fuelType;
    private String engine;
    private int horsePower;
    private String gearBox;
    private String description;
    private int distance;

    private Integer agreementType;
    private Integer owner;

    private BigDecimal amount;
    private String name;
    private String surname;
    private String adress;
    private String nip;
    private String pesel;
    private Integer carType;

    public NewCarDto() {
    }

    public Date getAdded() {
        return added;
    }

    public NewCarDto setAdded(Date added) {
        this.added = added;
        return this;
    }

    public Integer getOwner() {
        return owner;
    }

    public NewCarDto setOwner(Integer owner) {
        this.owner = owner;
        return this;
    }

    public Integer getCarType() {
        return carType;
    }

    public NewCarDto setCarType(Integer carType) {
        this.carType = carType;
        return this;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public NewCarDto setAmount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }

    public String getName() {
        return name;
    }

    public NewCarDto setName(String name) {
        this.name = name;
        return this;
    }

    public String getSurname() {
        return surname;
    }

    public NewCarDto setSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public String getAdress() {
        return adress;
    }

    public NewCarDto setAdress(String adress) {
        this.adress = adress;
        return this;
    }

    public String getNip() {
        return nip;
    }

    public NewCarDto setNip(String nip) {
        this.nip = nip;
        return this;
    }

    public String getPesel() {
        return pesel;
    }

    public NewCarDto setPesel(String pesel) {
        this.pesel = pesel;
        return this;
    }

    public Integer getId() {
        return id;
    }

    public NewCarDto setId(Integer id) {
        this.id = id;
        return this;
    }

    public String getVin() {
        return vin;
    }

    public NewCarDto setVin(String vin) {
        this.vin = vin;
        return this;
    }

    public String getYear() {
        return year;
    }

    public NewCarDto setYear(String year) {
        this.year = year;
        return this;
    }

    public String getMark() {
        return mark;
    }

    public NewCarDto setMark(String mark) {
        this.mark = mark;
        return this;
    }

    public String getModel() {
        return model;
    }

    public NewCarDto setModel(String model) {
        this.model = model;
        return this;
    }

    public String getLiabilityNumber() {
        return liabilityNumber;
    }

    public NewCarDto setLiabilityNumber(String liabilityNumber) {
        this.liabilityNumber = liabilityNumber;
        return this;
    }

    public String getRegistartionNumber() {
        return registartionNumber;
    }

    public NewCarDto setRegistartionNumber(String registartionNumber) {
        this.registartionNumber = registartionNumber;
        return this;
    }

    public String getFuelType() {
        return fuelType;
    }

    public NewCarDto setFuelType(String fuelType) {
        this.fuelType = fuelType;
        return this;
    }

    public String getEngine() {
        return engine;
    }

    public NewCarDto setEngine(String engine) {
        this.engine = engine;
        return this;
    }

    public int getHorsePower() {
        return horsePower;
    }

    public NewCarDto setHorsePower(int horsePower) {
        this.horsePower = horsePower;
        return this;
    }

    public String getGearBox() {
        return gearBox;
    }

    public NewCarDto setGearBox(String gearBox) {
        this.gearBox = gearBox;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public NewCarDto setDescription(String description) {
        this.description = description;
        return this;
    }

    public int getDistance() {
        return distance;
    }

    public NewCarDto setDistance(int distance) {
        this.distance = distance;
        return this;
    }

    public Integer getAgreementType() {
        return agreementType;
    }

    public NewCarDto setAgreementType(Integer agreementType) {
        this.agreementType = agreementType;
        return this;
    }
}
