package pl.autokomis.Dto;

import java.util.Date;

public class PersonDto {

    private Integer id;
    private String name;
    private String surname;
    private String adress;
    private String nip;
    private String pesel;

    private Integer userId;

    private String removed;

    private Date added;
    private String username;
    private String password;

    private String email;


    public PersonDto() {
    }

    public String getRemoved() {
        return removed;
    }

    public PersonDto setRemoved(String removed) {
        this.removed = removed;
        return this;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getPesel() {
        return pesel;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public Integer getUserId() {
        return userId;
    }

    public PersonDto setUserId(Integer userId) {
        this.userId = userId;
        return this;
    }

    public Date getAdded() {
        return added;
    }

    public PersonDto setAdded(Date added) {
        this.added = added;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public PersonDto setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public PersonDto setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public PersonDto setEmail(String email) {
        this.email = email;
        return this;
    }
}
