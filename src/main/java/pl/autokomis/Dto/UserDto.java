package pl.autokomis.Dto;



import java.util.Date;
import java.util.List;

public class UserDto {

    private Integer id;
    private Date added;
    private String username;
    private String password;
    private String email;

    private String token;

    private Integer personId;

    private String removed;


    private List<RoleTypeDto> roleType;

    public List<RoleTypeDto> getRoleType() {
        return roleType;
    }

    public UserDto setRoleType(List<RoleTypeDto> roleType) {
        this.roleType = roleType;
        return this;
    }

    public String getRemoved() {
        return removed;
    }

    public UserDto setRemoved(String removed) {
        this.removed = removed;
        return this;
    }


    public UserDto() {
    }

    public Integer getPersonId() {
        return personId;
    }

    public UserDto setPersonId(Integer personId) {
        this.personId = personId;
        return this;
    }

    public String getToken() {
        return token;
    }

    public UserDto setToken(String token) {
        this.token = token;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getAdded() {
        return added;
    }

    public void setAdded(Date added) {
        this.added = added;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
