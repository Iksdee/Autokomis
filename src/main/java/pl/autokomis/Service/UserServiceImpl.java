package pl.autokomis.Service;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import pl.autokomis.Dto.PersonDto;
import pl.autokomis.Dto.RoleTypeDto;
import pl.autokomis.Dto.UserDto;
import pl.autokomis.Entity.Person;
import pl.autokomis.Entity.RoleType;
import pl.autokomis.Entity.User;
import pl.autokomis.Repository.PersonRepository;
import pl.autokomis.Repository.RoleTypeRepository;
import pl.autokomis.Repository.UserRepository;

import javax.jws.soap.SOAPBinding;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
    public class UserServiceImpl implements UserService {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleTypeRepository roleTypeRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public List<PersonDto> getPersonList(){
        List<Person> personList = personRepository.findAll();
        List<PersonDto> personDtos = personList.stream().map(p-> getPersonList(p)).collect(Collectors.toList());
        return personDtos;
    }


    public boolean getUsersChecked( String userDtoUsername){
        List<User> userList = userRepository.findAll();
        Predicate<User> p1 = s -> s.getUsername().equals(userDtoUsername);
        return userList.stream().anyMatch(p1);
    }

    public boolean checkedUserAccount(String username){
        User user = userRepository.findByUsername(username);
        if(user.getPerson() == null)
            return false;
        return true;
    }



    public boolean getUserMailChecked(String userDtoMail){
        List<User> userList = userRepository.findAll();
        Predicate<User> p1 = s -> s.getEmail().equals(userDtoMail);
        return userList.stream().anyMatch(p1);
    }

    public UserDto getUserById(Integer id){
        User user = userRepository.findOne(id);
        List<RoleType> roleType = user.getRoleType();
        List<RoleTypeDto> roleTypeDto = roleType.stream().map(p-> getRoleLists(p)).collect(Collectors.toList());
        UserDto userDto = getUserList(user);
        userDto.setRoleType(roleTypeDto);
        return userDto;
    }


    public void saveUser(UserDto userDto, String token){
        RoleType roleType = roleTypeRepository.findByName("USER");
        List<RoleType> roleTypes = new ArrayList<>();
        roleTypes.add(roleType);
        ModelMapper modelMapper = new ModelMapper();
        User user = modelMapper.map(userDto, User.class);
        user.setToken(token);
        user.setRoleType(roleTypes);
        user.setPassword(bCryptPasswordEncoder.encode(userDto.getPassword()));
        user.setAdded(new Date());
        userRepository.save(user);
    }

    public void confirmAccount(String token){
        User user = userRepository.findByToken(token);
        user.setToken(null);
        userRepository.save(user);
    }



    public void deleteUserById(Integer id){
        User user = userRepository.findOne(id);
        user.setRemoved("deleted");
        userRepository.save(user);
    }


    public  void blockUserById(Integer id){
        User user = userRepository.findOne(id);
        user.setToken("banned");
        userRepository.save(user);
    }



    private PersonDto getPersonList(Person person){
        ModelMapper modelMapper = new ModelMapper();
        PersonDto personDto = modelMapper.map(person, PersonDto.class);
        return personDto;
    }



    private RoleTypeDto getRoleLists(RoleType roleType){
        ModelMapper modelMapper = new ModelMapper();
        RoleTypeDto roleTypeDto = modelMapper.map(roleType, RoleTypeDto.class);
        return roleTypeDto;
    }


    private UserDto getUserList(User user){
        ModelMapper modelMapper = new ModelMapper();
        UserDto userDto = modelMapper.map(user, UserDto.class);
        return userDto;
    }

    public List<UserDto> getUserListInfo(){
        List<User> userList = userRepository.findAll();
        List<UserDto> personDtos = userList.stream().map(p-> getUserList(p)).collect(Collectors.toList());
        return personDtos;
    }




}
