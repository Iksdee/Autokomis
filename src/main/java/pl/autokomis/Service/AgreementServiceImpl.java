package pl.autokomis.Service;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.autokomis.Dto.AgreementDto;
import pl.autokomis.Dto.AgreementTypeDto;
import pl.autokomis.Dto.NewCarDto;
import pl.autokomis.Entity.Agreement;
import pl.autokomis.Entity.AgreementType;
import pl.autokomis.Entity.Car;
import pl.autokomis.Entity.Person;
import pl.autokomis.Repository.AgreementRepository;
import pl.autokomis.Repository.AgreementTypeRepository;
import pl.autokomis.Repository.CarRepository;
import pl.autokomis.Repository.PersonRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AgreementServiceImpl implements AgreementService {

    @Autowired
    private AgreementRepository agreementRepository;

    @Autowired
    private AgreementTypeRepository agreementTypeRepository;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private CarRepository carRepository;




    public List<AgreementDto> getAgreementList(){
        List<Agreement> agreements = agreementRepository.findAll();
        List<AgreementDto> agreementDtos = agreements.stream().map(p-> getAgreementDto(p)).collect(Collectors.toList());
        return agreementDtos;
    }


    public List<AgreementTypeDto> getAgreementTypeList(){
        List<AgreementType> agreementsType = agreementTypeRepository.findAll();
        List<AgreementTypeDto> agreementTypeDtos = agreementsType.stream().map(p-> getAgreementTypeDto(p)).collect(Collectors.toList());
        agreementTypeDtos.remove(0);
        return agreementTypeDtos;
    }


    public void saveAgreement(NewCarDto newCarDto, Integer id){
        Agreement agreement = new Agreement();
        AgreementType agreementType = agreementTypeRepository.findOne(newCarDto.getAgreementType());
        Person person = personRepository.findOne(newCarDto.getOwner());
        agreement.setAgreementType(agreementType);
        agreement.setPerson(person);
        agreement.setDescription("Sprzedaż samochodu");
        agreement.setAmount(newCarDto.getAmount());
        agreement.setAdded(newCarDto.getAdded());
        Car car = carRepository.findOne(id);
        agreement.setCar(car);
        agreementRepository.save(agreement);
    }




    private AgreementDto getAgreementDto(Agreement agreement){
        ModelMapper modelMapper = new ModelMapper();
        AgreementDto agreementDto = modelMapper.map(agreement, AgreementDto.class);
        return agreementDto;
    }

    private AgreementTypeDto getAgreementTypeDto(AgreementType agreementType){
        ModelMapper modelMapper = new ModelMapper();
        AgreementTypeDto agreementTypeDto = modelMapper.map(agreementType, AgreementTypeDto.class);
        return agreementTypeDto;
    }

}
