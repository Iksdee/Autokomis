package pl.autokomis.Service;

import pl.autokomis.Dto.AgreementDto;
import pl.autokomis.Dto.AgreementTypeDto;
import pl.autokomis.Dto.NewCarDto;

import java.util.List;

public interface AgreementService {

     List<AgreementDto> getAgreementList();

     List<AgreementTypeDto> getAgreementTypeList();

      void saveAgreement(NewCarDto newCarDto, Integer id);

}
