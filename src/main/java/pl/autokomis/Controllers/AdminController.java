package pl.autokomis.Controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.autokomis.Dto.PersonDto;
import pl.autokomis.Dto.RoleTypeDto;
import pl.autokomis.Dto.UserDto;
import pl.autokomis.Service.PersonService;
import pl.autokomis.Service.UserService;

import java.util.List;

@Controller
public class AdminController {


    public final String USER_LIST ="User/userList";
    public final String USER_DETAILS ="User/userDetails";
    public final String USER_DETAILS_DEF ="User/userDetailsDef";

    @Autowired
    private UserService userService;

    @Autowired
    private PersonService personService;



    /**
     * Method showUserList - get user list from database.
     * @return  user list
     * */
    @GetMapping("/userList")
    public String showUserList(ModelMap modelMap){
        List<UserDto> userDtos = userService.getUserListInfo();
        modelMap.addAttribute("userList", userDtos);
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();
        modelMap.addAttribute("yourUsername", name);
        return USER_LIST;
    }


    /**
     * Method showUserList - get a details of user from user list.
     * @return  user details
     * */
    @GetMapping("/userList/details")
    public String showUserList(@RequestParam("id") Integer id, ModelMap modelMap){

        if(id == null || id <= 0){
            throw new RuntimeException("Błąd identyfikatora osobowego");
        }
        PersonDto personDto = personService.getPersonDetails(id);
        if(personDto == null){
            modelMap.put("message", "Brak informacji");
            return USER_DETAILS_DEF;
        }
        UserDto userDto = userService.getUserById(id);
        List<RoleTypeDto> roleTypeDtos = userDto.getRoleType();
        modelMap.addAttribute("userList", userDto);
        modelMap.addAttribute("roleList", roleTypeDtos);
        modelMap.put("userDetails", personDto);
        return USER_DETAILS;
    }

    /**
     * Method deleteSelectUser - delete a user (change flag).
     * @return  user list
     * */
    @GetMapping("/userList/details/delete")
    public String deleteSelectUser(@RequestParam("id") Integer id, ModelMap modelMap){
        if(id == null || id <= 0){
            throw new RuntimeException("Błąd identyfikatora osobowego");
        }
        userService.deleteUserById(id);
        return "redirect:/userList";
    }

    /**
     * Method blockSelectedUser - block a selected  user (change flag - token).
     * @return  user list
     * */

    @GetMapping("/userList/details/block")
    public String blockSelectedUser(@RequestParam("id") Integer id, ModelMap modelMap){
        if(id == null || id <= 0){
            throw new RuntimeException("Błąd identyfikatora osobowego");
        }
        userService.blockUserById(id);
        return "redirect:/userList";
    }

}
